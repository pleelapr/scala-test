import java.io.File
import com.typesafe.config._


class ApplicationConfigAccess (pathname: String){

  val parsedConfig = ConfigFactory.parseFile(new File("src/main/resources/application.conf"))
  val config = ConfigFactory.load(parsedConfig)

  config.checkValid(ConfigFactory.defaultReference(), pathname)

  // note that these fields are NOT lazy, because if we're going to
  // get any exceptions, we want to get them on startup.
  val url = config.getString(pathname+".url")
  val username = config.getString(pathname+".user")
  val password = config.getString(pathname+".password")
  val driver = config.getString(pathname+".driver")

  def printSetting(): Unit = {
    println("url=" + url)
    println("username=" + username)
    println("password=" + password)
    println("driver=" + driver)
  }

}
