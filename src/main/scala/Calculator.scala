import cats.Applicative
import cats.implicits._
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.{Encoder, Json}
import org.http4s.EntityEncoder
import org.http4s.circe._
import Utility._
import MessageMatching._

trait Calculator[F[_]]{
    def calculate(operation: Calculator.Operation, firstNumber: Calculator.FirstNumber, secondNumber: Calculator.SecondNumber): F[Calculator.Calculating]
}

object Calculator {
    implicit def apply[F[_]](implicit ev: Calculator[F]): Calculator[F] = ev

//    case class ParseOp[T](op: String => T)
//    implicit val popDouble = ParseOp[Double](_.toDouble)
//    implicit val popInt = ParseOp[Int](_.toInt)case class ParseOp[T](op: String => T)
//    implicit val popDouble = ParseOp[Double](_.toDouble)
//    implicit val popInt = ParseOp[Int](_.toInt)

//    def parse[T: ParseOp](s: String) = try { Some(implicitly[ParseOp[T]].op(s)) } catch {case _: Throwable => None}


    case class Formula(firstNumber: String, secondNumber: String, operation: String, result: String)
    final case class Operation(operation: String) extends AnyVal
    final case class FirstNumber(firstNumber: String) extends AnyVal
    final case class SecondNumber(secondNumber: String) extends AnyVal
    /**
     * More generally you will want to decouple your edge representations from
     * your internal data structures, however this shows how you can
     * create encoders for your data.
     **/
    final case class Calculating(result: MessageObject) extends AnyVal
    object Calculating {
        implicit val greetingEncoder: Encoder[Calculating] = new Encoder[Calculating] {
            final def apply(a: Calculating): Json = Json.obj(
                ("code", Json.fromString(a.result.messageCode)),
                ("message", Json.fromString(a.result.messageHeader)),
            )
        }
        implicit def greetingEntityEncoder[F[_]: Applicative]: EntityEncoder[F, Calculating] =
            jsonEncoderOf[F, Calculating]
    }

    def checkList(l: List[Either[MessageObject, _]]): Either[MessageObject, String] = {
        l match {
            case Left(err) :: xs =>
                Left(err)
            case Right(_) :: xs =>
                checkList(xs)
            case Nil =>
                Right("No Error")
        }

    }

    def impl[F[_]: Applicative]: Calculator[F] = new Calculator[F]{
        def calculate(operation: Calculator.Operation, firstNumber: Calculator.FirstNumber, secondNumber: Calculator.SecondNumber): F[Calculator.Calculating] = {

            val firstNum = parseDouble(firstNumber.firstNumber)
            val secondNum = parseDouble(secondNumber.secondNumber)
            val numbers = List[Either[MessageObject, _]](firstNum, secondNum)
            var response = ResultMessage("")

            checkList(numbers) match {
                case Left(e) => response = e;
                case _ => response = performCalculation()
            }

            def performCalculation(): MessageObject = {
                val temp_result: Any = operation.operation match {
                    case "add" => add(firstNum.getOrElse(0), secondNum.getOrElse(0))
                    case "sub" => substract(firstNum.getOrElse(0), secondNum.getOrElse(0))
                    case "mul" => multiply(firstNum.getOrElse(0), secondNum.getOrElse(0))
                    case "div" => divide(firstNum.getOrElse(0), secondNum.getOrElse(0))
                    case _ => Nil
                }
                val operationSymbol: Any = operation.operation match {
                    case "add" => "+"
                    case "sub" => "-"
                    case "mul" => "*"
                    case "div" => "/"
                    case _ => Nil
                }
                val r = Formula(firstNum.toString, secondNum.toString, operationSymbol.toString,
                    temp_result.toString).asJson.toString()
                ResultMessage(r)
            }
            Calculating(response).pure[F];
        }
    }

    def add(x: Double, y: Double): Double ={
        x + y
    }

    def substract(x: Double, y: Double): Double = {
        x - y
    }

    def multiply(x: Double, y: Double): Double = {
        x * y
    }

    def divide(x: Double, y: Double): Double = {
        x / y
    }

    def checkOperationValid(op: String): Boolean = {
        val validOperation = List("add", "sub", "mul", "div")
        if(validOperation.contains(op)){
            return true
        }
        else{
            return false
        }
    }

//    def parseDouble(s: String) = try { Some(s.toDouble) } catch { case _: Throwable => None }

//    def parseDouble2(s: String): Either[String, Double] = s.toDouble match {
//        case d: Double => Right(d)
//        case _ => Left("Error")
//    }

    def checkDoubleValid(str: String): Boolean = {
        if (parseDouble(str) == None){
            return false
        } else {
            return true
        }
    }

    def Operation(x: Double, y: Double, op: String): Double ={
        val temp_result: Double = op match {
            case "add" => (x + y)
            case "sub" => (x - y)
            case "mul" => (x * y)
            case "div" => (x / y)
        }
        print(s"$x $op $y = $temp_result")
        return temp_result
    }
}