import cats.Applicative
import cats.effect.{Blocker, IO}
import doobie.Transactor
import io.circe.{Encoder, Json}
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf
import doobie.implicits._
import doobie.util.ExecutionContexts
import cats.implicits._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization.write
import Utility._
import MessageMatching._


//import scala.util.Try

case class Employee(id: Int, first_name: String, last_name: String, email: String, gender: String, ip_address: String)

trait EmployeeDBConnection[F[_]]{
    def queryByName(name: EmployeeDBConnection.Name): F[EmployeeDBConnection.Querying]
    def query(id: EmployeeDBConnection.ID): F[EmployeeDBConnection.Querying]
}


object EmployeeDBConnection {

    implicit def apply[F[_]](implicit ev: Calculator[F]): Calculator[F] = ev

    final case class ID(ID: String) extends AnyVal
    final case class Name(Name: String) extends AnyVal

    final case class Querying(result: MessageObject) extends AnyVal

    object Querying {
        implicit val resultEncoder: Encoder[Querying] = new Encoder[Querying] {
//            final def apply(a: Querying): Json = Json.obj(
//                ("message", Json.fromString(a.result)),
//            )
            final def apply(a: Querying): Json = Json.obj(
                ("code", Json.fromString(a.result.messageCode)),
                ("message", Json.fromString(a.result.messageHeader)),
            )
        }

        implicit def resultEntityEncoder[F[_]: Applicative]: EntityEncoder[F, Querying] =
            jsonEncoderOf[F, Querying]
    }

    def impl[F[_]: Applicative]: EmployeeDBConnection[F] = new EmployeeDBConnection[F]{
        def query(id: EmployeeDBConnection.ID): F[EmployeeDBConnection.Querying] = {
            val employeeQuery = new EmployeeQuery
            val response = employeeQuery.queryOnEmployee(id.ID)
            response match {
                case Right(r) => Querying(r).pure[F]
                case Left(l) => Querying(l).pure[F]
            }

        }
        def queryByName(name: EmployeeDBConnection.Name): F[EmployeeDBConnection.Querying] = {
            val employeeQuery = new EmployeeQuery
            val response = employeeQuery.queryEmployeeWithName(name.Name)
            response match {
                case Right(r) => r match {
                    case "[]" =>Querying(ResultNotFoundError).pure[F]
                    case _ => Querying(r).pure[F]
                }
                case Left(l) => Querying(l).pure[F]
            }
        }
    }

}



class EmployeeQuery {


    //Get App Config
    val settings = new ApplicationConfigAccess("postgresEmployee")

    implicit val cs = IO.contextShift(ExecutionContexts.synchronous)

    //Import Doobie Transactor
    val xa = Transactor.fromDriverManager[IO](
        settings.driver,     // driver classname
        settings.url,     // connect URL (driver-specific)
        settings.username,                  // user
        settings.password,                          // password
        Blocker.liftExecutionContext(ExecutionContexts.synchronous) // just for testing
    )

    //Define Query for Employee Database
    def getEmployeeByIDQuery(s: String) = {
        //        sql"""SELECT id, first_name, last_name, gender, email, ip_address FROM employee WHERE id='$id'""".query[Employee]
        val sInt = parseInt(s)
        val a = fr"SELECT id, first_name, last_name, gender, email, ip_address FROM employee"
        sInt match {
            case Right(i) => println((a ++ fr"where id = $i;")); Right((a ++ fr"where id = $i;").query[Employee])
            case Left(e) => Left(e)
        }
    }

    def getEmployeeByNameQuery(Name: String) = {
        val checkedName = checkEmployeeName(Name)

        val a = fr"SELECT id, first_name, last_name, gender, email, ip_address FROM employee"
        checkedName match {
            case Right(i) =>
                val i_temp = "%"+i+"%"
                val c = a ++ fr"WHERE first_name LIKE $i_temp OR last_name LIKE $i_temp;"
                println(c)
                Right(c.query[Employee])
            case Left(e) => Left(e)
        }
    }

    def queryEmployeeWithName(Name: String): Either[MessageObject, String] = {
        val queryString = getEmployeeByNameQuery(Name)
        implicit val formats: DefaultFormats = DefaultFormats
        queryString match {
            case Right(r) => Right(write(r.to[List].transact(xa).unsafeRunSync))
            case Left(e) => Left(e)
        }
    }

    //Run Queries
    def queryOnEmployee(id: String): Either[MessageObject, String] = {
        val queryString = getEmployeeByIDQuery(id)

        implicit val formats: DefaultFormats = DefaultFormats

        queryString match {
            case Right(r) => Right(write(r.to[List].transact(xa).unsafeRunSync))
            case Left(e) => Left(e)
        }
    }

}
