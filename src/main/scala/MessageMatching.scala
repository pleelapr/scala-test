abstract class MessageObject{
    def messageCode: String
    def messageHeader: String
}

object MessageMatching{
    implicit def ResultMessage(msg: String) = new MessageObject {
        override def messageCode: String = "200"
        override def messageHeader: String = msg
    }
    implicit def ParseIntError = new MessageObject {
        override def messageCode: String = "P301"
        override def messageHeader: String = "Parse Int Error"
    }
    implicit def ParseDoubleError = new MessageObject {
        override def messageCode: String = "P302"
        override def messageHeader: String = "Parse Double Error"
    }
    implicit def InvalidCountryCodeError = new MessageObject {
        override def messageCode: String = "E001"
        override def messageHeader: String = "Invalid Country Code"
    }
    implicit def ResultNotFoundError = new MessageObject {
        override def messageCode: String = "N001"
        override def messageHeader: String = "No result is found"
    }
    implicit def InvalidEmployeeName = new MessageObject {
        override def messageCode: String = "E002"
        override def messageHeader: String = "Invalid Employee Name"
    }
}

