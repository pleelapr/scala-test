import cats.effect.Sync
import cats.implicits._
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
//import org.json4s.DefaultFormats

object ScalaRoute {
    def helloWorldRoutes[F[_]: Sync](H: HelloWorld[F]): HttpRoutes[F] = {
        val dsl = new Http4sDsl[F]{}
        import dsl._
        HttpRoutes.of[F] {
            case GET -> Root / "hello" / name =>
                for {
                    greeting <- H.hello(HelloWorld.Name(name))
                    resp <- Ok(greeting)
                } yield resp
        }
    }
    def calculatorhttpRoutes[F[_]: Sync](C: Calculator[F]): HttpRoutes[F] = {
        val dsl = new Http4sDsl[F]{}
        import dsl._
        HttpRoutes.of[F] {
            case GET -> Root / "calculator" / operation / firstnumber / secondnumber =>
                for {
                    greeting <- C.calculate(Calculator.Operation(operation), Calculator.FirstNumber(firstnumber), Calculator.SecondNumber(secondnumber))
                    resp <- Ok(greeting)
                } yield resp
        }
    }
    def EmployeeDBRoutes[F[_]: Sync](C: EmployeeDBConnection[F]): HttpRoutes[F] = {
        val dsl = new Http4sDsl[F]{}
//        implicit val formats: DefaultFormats = DefaultFormats
        import dsl._
        HttpRoutes.of[F] {
            case GET -> Root / "employee" / "id" / id =>
                for {
                    response <- C.query(EmployeeDBConnection.ID(id))
                    resp <- Ok(response)
                } yield resp

            case GET -> Root / "employee" / "name" / name =>
                for {
                    response <- C.queryByName(EmployeeDBConnection.Name(name))
                    resp <- Ok(response)
                } yield resp
        }
    }

    def WorldDBRoutes[F[_]: Sync](C: WorldDBConnection[F]): HttpRoutes[F] = {
        val dsl = new Http4sDsl[F]{}
        import dsl._
        HttpRoutes.of[F] {
            case GET -> Root / "world" / "id" / id =>
                for {
                    response <- C.query(WorldDBConnection.ID(id))
                    resp <- Ok(response)
                } yield resp
        }
    }

}
