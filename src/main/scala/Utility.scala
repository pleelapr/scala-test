import MessageMatching._

trait parseStuff[T] {
    def parseInt(s: T): Either[MessageObject, Int]
    def parseDouble(s: T): Either[MessageObject, Double]
}

trait checkingValid[T] {
    def checkCountryCode(s: T): Either[MessageObject, String]
    def checkEmployeeName(s: T): Either[MessageObject, String]
}

object Utility extends parseStuff[String] with checkingValid[String] {

    implicit def parseInt(s: String): Either[MessageObject, Int] = {
        s.toIntOption match {
            case Some(i) => Right(i)
            case None => Left(ParseIntError)
        }
    }

    implicit def parseDouble(s: String): Either[MessageObject, Double] = {
        s.toDoubleOption match {
            case Some(i) => Right(i)
            case None => Left(ParseDoubleError)
        }
    }

    implicit def checkCountryCode(s: String): Either[MessageObject, String] = {
        val Pattern = "([A-Z]{3})".r
        s match {
            case Pattern(c) => Right(c)
            case _ => Left(InvalidCountryCodeError)
        }
    }

    implicit def checkEmployeeName(s: String): Either[MessageObject, String] = {
        println(s)
        val Pattern = "^([A-Z][a-zA-Z]{1,30})$".r
        s match {
            case Pattern(c) => Right(c)
            case _ => Left(InvalidEmployeeName)
        }
    }
}
