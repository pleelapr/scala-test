import Utility._
import cats.Applicative
import cats.effect.{Blocker, IO}
import doobie.Transactor
import io.circe.{Encoder, Json}
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf
import doobie.implicits._
import doobie.util.ExecutionContexts
import cats.implicits._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization.write
import MessageMatching._

//import org.apache.commons.lang.StringEscapeUtils

case class Country(code: String, name: String, pop: Int, gnp: Option[Double])

trait WorldDBConnection[F[_]]{
  def query(id: WorldDBConnection.ID): F[WorldDBConnection.Querying]
}


object WorldDBConnection {

  implicit def apply[F[_]](implicit ev: Calculator[F]): Calculator[F] = ev

  final case class ID(ID: String) extends AnyVal

  final case class Querying(result: MessageObject) extends AnyVal

  object Querying {
    implicit val resultEncoder: Encoder[Querying] = new Encoder[Querying] {
      //            final def apply(a: Querying): Json = Json.obj(
      //                ("message", Json.fromString(a.result)),
      //            )
      final def apply(a: Querying): Json = Json.obj(
        ("code", Json.fromString(a.result.messageCode)),
        ("message", Json.fromString(a.result.messageHeader)),
      )
    }

    implicit def resultEntityEncoder[F[_]: Applicative]: EntityEncoder[F, Querying] =
      jsonEncoderOf[F, Querying]
  }

  def impl[F[_]: Applicative]: WorldDBConnection[F] = new WorldDBConnection[F]{
    def query(id: WorldDBConnection.ID): F[WorldDBConnection.Querying] = {
      val worldQuery = new WorldQuery
      val response = worldQuery.queryOnCountry(id.ID)
      var msg = ResultMessage("")
      response match {
        case Right(r) =>
          r match {
            case "[]" => msg = ResultNotFoundError
            case _ => msg = r
          }
        case Left(l) => msg = l
      }
      Querying(msg).pure[F]
    }
  }



}

class WorldQuery {
  //Get App Config
  val settings = new ApplicationConfigAccess("postgresWorld")

  implicit val cs = IO.contextShift(ExecutionContexts.synchronous)

  //Import Doobie Transactor
  val xa = Transactor.fromDriverManager[IO](
    settings.driver,     // driver classname
    settings.url,     // connect URL (driver-specific)
    settings.username,                  // user
    settings.password,                          // password
    Blocker.liftExecutionContext(ExecutionContexts.synchronous) // just for testing
  )

  //Define Query for Employee Database
  def getCountryByIDQuery(id: String) = {
    val sInt = checkCountryCode(id)
    val a = fr"select code, name, population, gnp from country"
    sInt match {
      case Right(i) => Right((a ++ fr"where code = $i;").query[Country])
      case Left(e) => Left(e)
    }
  }

  //Run Queries
  def queryOnCountry(id: String): Either[MessageObject, String] = {
    val queryString = getCountryByIDQuery(id)
    implicit val formats: DefaultFormats = DefaultFormats

    queryString match {
      case Right(r) => Right(write(r.to[List].transact(xa).unsafeRunSync))
      case Left(e) => Left(e)
    }


  }

}
